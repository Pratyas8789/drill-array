function find(elements, cb) {
    if (!Array.isArray(elements) || typeof cb !== 'function' || elements.length === 0) {
        return undefined;
    }
    let ans;
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index])) {
            ans = elements[index];
            return ans;
        }
    }
    return ans;

}

module.exports = find;