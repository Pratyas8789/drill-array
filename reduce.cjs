function reduce(elements, cb, startingValue) {
    if (!Array.isArray(elements) || typeof cb !== 'function' || elements.length === 0) {
        return undefined;
    }

    let acc=startingValue;
    let start=0;
    if(startingValue=== undefined){
        acc=elements[0];
        start=1;
    }
    for(let index=start; index<elements.length;index++){
        acc=cb(acc ,elements[index],index,elements);
    }
    return acc;
}

module.exports = reduce;