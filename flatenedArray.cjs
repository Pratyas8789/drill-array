const arr=[];

function flatten(elements) {
    if (!Array.isArray(elements)) {
        return [];
    }
    for(let ele in elements){
        if(Array.isArray(elements[ele])){
            flatten(elements[ele]);
        }else{
            arr.push(elements[ele]);
        }
    }
    return arr;
}

module.exports = flatten;