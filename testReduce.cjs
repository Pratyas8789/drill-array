const reduceFunction=require('./reduce.cjs')
const givenData=require('./arrays.cjs')

const givenDataItems=givenData['items']

function cb(acc, ele,index,array) {
    return acc+ele;
}

result = reduceFunction(givenDataItems,cb,2);

console.log(result)
