const findFunction=require('./find.cjs')
const givenData=require('./arrays.cjs')

const givenDataItems=givenData['items']

function cb(element) {
    return element === 5;
}

const result = findFunction(givenDataItems,cb);

console.log(result)

